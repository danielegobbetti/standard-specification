[[anchor-D]]
== D. External links

[[anchor-footnote0]]
^[0]^ *Creative Commons BY SA license:* +
[small]#https://creativecommons.org/licenses/by-sa/3.0/#

[[anchor-footnote1]]
^[1]^ *HTTP basic access authentication:* +
[small]#https://en.wikipedia.org/wiki/Basic_access_authentication/#

[[anchor-footnote2]]
^[2]^ *OpenTravel Alliance:* +
[small]#https://opentravel.org/#

[[anchor-footnote3]]
^[3]^ *OTA2015A documentation:* +
[small]#http://opentravelmodel.net/pubs/specifications/OnlinePublicationDetails.html?spec=2015A&amp;specType=1_0&amp;group=19701#

[[anchor-footnote4]]
^[4]^ *OTA2015A XML schema files:* +
[small]#http://opentravelmodel.net/pubs/specifications/OnlinePublicationDetails.html?spec=2015A&amp;specType=OTA_1_0#

[[anchor-footnote5]]
^[5]^ *OTA2015A code list:* +
[small]#http://opentravelmodel.net/pubs/specifications/OnlinePublicationDetails.html?spec=2015A&amp;specType=1_0&amp;group=19708#

[[anchor-footnote6]]
^[6]^ *browsable interface to the OTA XML schema files (choose model "OTA2015A"):* +
[small]#https://www.pilotfishtechnology.com/modelviewers/OTA/#
