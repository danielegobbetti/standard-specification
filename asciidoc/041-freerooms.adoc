[[anchor-4.1]]
=== 4.1. FreeRooms: room availability notifications

When the value of the [request_param]`action` parameter is `OTA_HotelAvailNotif:FreeRooms` the client intends to send room availability notifications to the server.

A server that supports this action *must* support at least one of two capabilities: `OTA_HotelAvailNotif_accept_rooms` or `OTA_HotelAvailNotif_accept_categories`.
This way the server indicates whether it can handle the availability of rooms at the level of distinct rooms, at the level of categories of rooms or both.


[[anchor-4.1.1]]
==== 4.1.1. Client request

The parameter [request_param]`request` must contains an `OTA_HotelAvailNotifRQ` document.

Clients and servers typically wish to exchange only delta information about room availabilities in order to keep the total amount of data to be processed in check.

However, for simplicity let us first consider a request where the client transmits the complete availability information as might be the case for a first synchronization.

Consider the outer part of the example document:

|===
a[freerooms_bkg]|
[source,xml]
----
include::includes/4.1.1-FreeRooms-OTA_HotelAvailNotifRQ-outer.txt[]
----
v| [small]**`samples/FreeRooms-OTA_HotelAvailNotifRQ.xml`** - outer part
|===
An `OTA_HotelAvailNotifRQ` may contain just *one* [xml_element_name]#AvailStatusMessages# (note the plural) element, hence at most *one* hotel can be dealt with in a single request.

The [xml_element_name]#UniqueID# element with attribute [xml_attribute_name]#Instance# = `CompleteSet` and [xml_attribute_name]#Type# = `16` indicates that this message contains the complete information (as entered by the user).
When receiving such a request, a server *must* remove all information about any availability it might have on record regarding the given hotel.

The attribute [xml_attribute_name]#ID# is needed for compatibility with OTA, the value is ignored by {AlpineBitsR}.

A client might want to let a server know its availability data should be purged based on internal business rules.
An example might be availability data that is considered stale, because it hasn’t been updated by the user for some time.
The client then *should* send a request using an [xml_element_name]#UniqueID# element with attribute [xml_attribute_name]#Instance# = `CompleteSet` and [xml_attribute_name]#Type# = `35`.
A server *must* accept this value (without returning an error or warning) and *could* make use of this hint and keep the availability data it has on record marking it as purged.
Otherwise such a message should be considered equivalent to the one with [xml_attribute_name]#Type# = `16`.

If the [xml_element_name]#UniqueID# element is missing, the message contains *delta information*. In that case the server updates only the information that is contained in the message without touching the other information that it has on record.

{AlpineBitsR} requires the attributes [xml_attribute_name]#HotelCode# *or* [xml_attribute_name]#HotelName# to be present (and match information in the server's database).
The fictitious hotel in the example is the "Frangart Inn" with code "123". Specifying both — code and name — is redundant, but allowed, as long as both are consistent.

{AlpineBitsR} *requires* a match of [xml_attribute_name]#HotelCode#, [xml_attribute_name]#HotelName# to be *case sensitive*.

Second, consider the inner part of the example that contains [xml_element_name]#AvailStatusMessage# (note the singular) elements for three different rooms.

Let's start with the availabilities for room 101S.

|===
a[freerooms_bkg]|
[source,xml]
----
include::includes/4.1.1-FreeRooms-OTA_HotelAvailNotifRQ-inner.txt[]
----
v|[small]**`samples/FreeRooms-OTA_HotelAvailNotifRQ.xml`** - inner part
|===

The use of the [xml_attribute_name]#InvCode# attribute tells us we're dealing with a *specific* room (101S) that belongs to the room category given by the [xml_attribute_name]#InvTypeCode# (double).

Alternatively, using a [xml_attribute_name]#InvTypeCode# without a [xml_attribute_name]#InvCode# attribute would indicate that the availability refers to a category of rooms, not a specific room.

{AlpineBitsR} *requires* a match of [xml_attribute_name]#InvCode# or [xml_attribute_name]#InvTypeCode# to be *case sensitive*.

An {AlpineBitsR} server *must* be able to treat *at least* one case out of the two cases (specific rooms or categories).
A client should perform the `getCapabilities` action to find out whether the server treats the room case (token `OTA_HotelAvailNotif_accept_rooms`), the category case (token `OTA_HotelAvailNotif_accept_categories`) or both.

Mixing rooms and categories in a single request is *not* allowed.
An {AlpineBitsR} server *must* return an error if it receives such a mixed request.

The attribute [xml_attribute_name]#Start# and [xml_attribute_name]#End# indicate that room 101S is available from 2010-08-01 to 2010-08-10 and from 2010-08-21 to 2010-08-30.

Regarding the first interval, this means the earliest possible check-in is 2010-08-01 afternoon and latest possible check-out is 2010-08-11 morning (maximum stay is 10 nights).

Check-ins *after* 2010-08-01 and stays of *less* than 10 nights are allowed as well, provided the check-out is not later than 2010-08-11 morning.

Idem for the other block of 10 nights from 2010-08-21 to 2010-08-30 (latest check-out is 2010-08-31 morning).

Note that {AlpineBitsR} does *not allow* [xml_element_name]#AvailStatusMessage# elements with overlapping periods.
This implies that the order of the [xml_element_name]#AvailStatusMessage# elements doesn't matter.
It is a *client's responsibility* to avoid overlapping.
An {AlpineBitsR} server's business logic *may* identify overlapping and return an error or *may* proceed in an implementation-specific way.

The integer value of the attribute [xml_attribute_name]#BookingLimit# indicates the number of available rooms.
Since in the example we’re dealing with a specific room here ([xml_attribute_name]#InvCode# is 101S), the only meaningful value of [xml_attribute_name]#BookingLimit# is 0 or 1 (the same room can not be available more than once).
In the category case, numbers larger than 1 would also be allowed.

[xml_attribute_name]#BookingLimit# numbers are always interpreted to be absolute numbers. Differential updates are not allowed.

A server *may* support handling rooms that are considered *free but not bookable* (see section <<anchor-4.2,GuestRequests>> for the description of bookings) and *must* set the `OTA_HotelAvailNotif_accept_BookingThreshold` capability accordingly.

If the capability is set, a client *must* send the [xml_attribute_name]#BookingThreshold# attribute in the [xml_element_name]#AvailStatusMessage# element.
A client that is only sending bookable rooms *must* set [xml_attribute_name]#BookingThreshold# to 0.

The number of bookable rooms are hence given by [xml_attribute_name]#BookingLimit# - [xml_attribute_name]#BookingThreshold#.

Since overbooking is not allowed and the number of bookable rooms cannot exceed the number of free rooms the inequality 0 ≤ [xml_attribute_name]#BookingThreshold# ≤ [xml_attribute_name]#BookingLimit# holds.

If the server does not have the capability set, a client *must not* send the [xml_attribute_name]#BookingThreshold# attribute in the [xml_element_name]#AvailStatusMessage# element, and all the rooms are implicitly considered bookable.

It also requires exactly one [xml_element_name]#StatusApplicationControl# element with attributes [xml_attribute_name]#Start#, [xml_attribute_name]#End#, [xml_attribute_name]#InvTypeCode# and (optional [xml_attribute_name]#InvCode#) for each [xml_element_name]#AvailStatusMessage# element.
It *must* return an error if any of these are missing.
There is however one exception: to completely reset all room availability information for a given Hotel a client might send a `CompleteSet` request with just one empty [xml_element_name]#AvailStatusMessage# element without any attributes.
The presence of the empty [xml_element_name]#AvailStatusMessage# element is required for OTA validation.

{AlpineBitsR} *recommends* that Implementers that use delta requests *should* send the full set of information periodically.

A server that supports delta requests *must* indicate so via the `OTA_HotelAvailNotif_accept_deltas` capability. As always, it is the *client's responsibility* to check whether the server supports deltas before trying to send them.


[[anchor-4.1.2]]
==== 4.1.2. Server response

The server will send a response indicating the outcome of the request.
The response is a `OTA_HotelAvailNotifRS` document.
Any of the four possible {AlpineBitsR} server response outcomes (success, advisory, warning or error)
are allowed.

See <<anchor-A,Appendix A>> for details.
 
 
[[anchor-4.1.3]]
==== 4.1.3. Implementation tips and best practice

* Note that in the 2011-11 version of {AlpineBitsR} the support of this action was mandatory for the server. This is no longer the case.

* Note that sending partial information (deltas) was added with {AlpineBitsR} 2013-04.

* For non-delta requests, since no time frame is explicitly transmitted by the client, a server is encouraged to delete and insert all the information stored in its backend, rather than updating it.

* Please note that the [xml_attribute_name]#End# date of an interval identifies the last day and night of the stay. Departure is the morning of the day *after* the [xml_attribute_name]#End# date.

* Please note that previous versions of {AlpineBitsR} allowed some booking restrictions to be used in FreeRooms (length of stay and day of arrival). This possibility has been removed with version 2014-04 as these restrictions are better handled by RatePlans.


