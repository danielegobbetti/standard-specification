<OTA_HotelRatePlanRQ xmlns="http://www.opentravel.org/OTA/2003/05" Version="3.000">
  <RatePlans>
    <RatePlan>

      <DateRange Start="2016-12-25" End="2017-01-03" />

      <RatePlanCandidates>
        <RatePlanCandidate RatePlanCode="DZ" RatePlanID="DailyRate"/>
        <RatePlanCandidate RatePlanCode="SPECIAL"/>
      </RatePlanCandidates>

      <HotelRef HotelCode="123" HotelName="Frangart Inn" />

    </RatePlan>
  </RatePlans>
</OTA_HotelRatePlanRQ>
