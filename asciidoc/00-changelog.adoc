[discrete]
== Document Change Log

Important note: make sure to have the latest version of this document! The latest version is available from https://www.alpinebits.org.

[cols="3,4,20",options="header"]
|===
|protocol version
|doc. release date
|description
|2018-10
|2018-11-30
a|Status:

* official release

Notable documentation changes:

* **The {AlpineBitsR} documentation is now edited using a public git repository, see the repository history for detailed changelog**

Updates and Addictions:

* Handshaking: the Housekeeping action has been renamed to Handshaking and the relative chapter has been completely rewritten introducing also breaking changes. Both server and client can now announce the capabilities they support.
* GuestRequests: it is now possible to push the GuestRequests instead of polling them.
* GuestRequests/Inventory: a new attribute has been added in order to allow a finer classification of the guest accommodation.
* Inventory: added the possibility to exchange references to the online presence of the lodging structure (e.g. social networks, review aggregators, etc.)
* RatePlans: the price calculation has been reviewed and simplified
* BaseRates: the interaction with the Channel Managers has been clarified and simplified
* Servers can now request the client to send a full data set in case of suspected mis-alignment of data

|2017-10
|2017-11-08
a|Status:

* official release

Notable documentation changes:

* **The {AlpineBitsR} documentation is now released under the Creative Commons Attribution-ShareAlike 3.0 Unported License**
* The attached Trademark policy is now an integral part of the {AlpineBitsR} specifications

Updates and additions:

* Chapter 2: added version handshake best practices
* FreeRooms: added transmission of number of bookable rooms, added purge of stale data
* Inventory: heavily updated and refactored
* GuestRequests: added Commission element, added encrypted credit card numbers and made card holder name optional, added hints on how to fill ReservationID
* RatePlans: added static rates, added supplements that are only available on given days of the week and supplements that depend on room category, improved and extended offers, added SetForwardMinStay and SetForwardMaxStay, extended descriptions and improved their documentation
* new action: BaseRates
* added general support of HTML in descriptions as an additional format (with some warnings)
* minor textual improvements

Reorganization of the appendixes:

* Appendix A: common section about  server response outcomes
* Appendix C: compatibility to previous versions described
* Appendix D: updated links to OTA2015A standard resources

Removals:

* removed SimplePackages

|2015-07b
|2016-08-01
a|Status:

* official release

Updates and additions:

* RatePlans: Section 4.5 has been rewritten to clarify details that the previous version just skipped over, including a detailed description of the price calculation algorithm
* RatePlans: new optional capability OTA_HotelRatePlanNotif_accept_RatePlanJoin to allow displaying alternative treatments for the same price list
* schema updates and validation: explicitly forbid some values (e.g. base prices of 0 EUR) and limit length of some attributes
* minor fixes and clarifications


|2015-07
|2015-10-28
a|Status:

* official release

Updates and additions:

* **OTA compatibility: version 2015A is now used**
* GuestRequests: a series to modifications and additions to make it more flexible especially for reservations
* GuestRequests: added refusals (warnings)
* GuestRequests: added booking modifications (ResStatus = 'Modify')
* RatePlans: changes to capabilities
* RatePlans: some clarifications and small additions 
* RatePlans: partial rewrite and better explanation of Supplements
* RatePlans: added the explicit response messages
* Inventory: changes to capabilities
* Inventory: replaced by OTA_HotelDescriptiveContentNotifRQ
* Inventory: added possibility to send multimedia content
* Inventory: added additional descriptive content that may be sent separately from the basic data


|2014-04
|2014-12-23
a|Status:

* official release with minor errata fixed
* section 4.2.3.: the example was not correct about the fact that the presence of a SelectionCriteria Start requires the server to send the list of inquiries again, regardless whether the client has retrieved them before or not (example fixed and misleading sentence removed)
* section 4.1.1: the document did not mention that it is allowed to send a single empty AvailStatusMessage element in a CompleteSet request to reset the room availabilities in a given Hotel - the empty AvailStatusMessage is required for OTA compatibility (this special case is now explicitly mentioned)
* section 4.12: in the table at the end of the section the code for “Invalid hotel” was wrongly given as 61 instead of 361 (typo fixed)
* section 4.5.2:  the document did not mention that it is allowed to send a single empty RatePlan element in a CompleteSet request to reset the rate plans in a given Hotel - the empty RatePlan is required for OTA compatibility (this special case is now explicitly mentioned)


|2014-04
|2014-10-15
a|Status:

* official release

Updates and additions:

* this is a major overhaul of {AlpineBitsR} - see appendix C.4 for a list of breaking changes, updates and additions
* new section: **Inventory** - room category information
* new section: **RatePlans**


|2013-04
|2013-05-24
a|Status:

* official release

Updates:

* Section 2 (HTTPS layer): added information regarding the new **X-AlpineBits-ClientID** and **X-AlpineBits-ClientProtocolVersion** fields in the HTTP header
* Section 3.2 (capabilities): added capability for FreeRooms deltas
* Section 4 (Intro): changed the text a bit to make it clearer that {AlpineBitsR} does indeed support booking requests and not only requests for quotes
* Section 4.1 (FreeRooms): added the possibility to send partial information (deltas); added warning response; much improved description of the response in general
* Section 4.2 (GuestRequests): slightly improved the description of the response in case of error
* Section 4.3 (Simple Packages): added limitation (just one Hotel per request); added warning response; much improved description of the response in general; clarified text to explicitly state that it is not allowed to mix package add and delete requests in a single message
* Appendix B: this document should be language neutral so the code that used to be here has been removed with a message to check the official {AlpineBitsR} site (with the current release the code is still in the documentation kit, however)
* Appendix C: new appendix

|2012-05b
|2012-10-01
a|Status:

* official release

Updates:

	* OTA compatibility: the attribute [xml_attribute_name]`Thu` is renamed to [xml_attribute_name]`Thur` and the attribute [xml_attribute_name]`Wed` is renamed to [xml_attribute_name]`Weds`
* **SimplePackages**: the element [xml_element_name]`Image` is listed as **mandatory** in the table as it already was in the text and schema files
* **SimplePackages**: The element [xml_element_name]`RateDescription` is listed as non-repeatable in the table as it already was in the text and schema files

|2012-05
|2012-05-31
a|Status:

* official release

Updates:

* major rewrite of the text
* FreeRooms: action OTA_HotelAvailNotif is no longer mandatory

Additions:

* GuestRequests: reservation inquiries
* SimplePackages: package availability notifications

|2011-11
|2011-11-18
a| minor alterations and release under Creative Commons Attribution-NoDerivs 3.0 Unported License
|2011-10
|2011-10-20
a| production release with minor alterations
|2011-09
|2011-09-08
a| first draft of redesigned version (using POST instead of SOAP)
|2010-10
|2010-10-20
a| second draft
|2010-08
|2010-08-01
a| first draft
|===
